using System;
using System.Collections.Generic;
using System.Text;

namespace _6_ZAD
{
    class Director
    {
        IBuilder Builder = new NotificationBuilder();
        public Director(IBuilder builder)
        {
            this.Builder = builder;
        }
        public void SetBulder(IBuilder builder)
        {
            this.Builder = builder;
        }
        public ConsoleNotification LogInfo(string author)
        {
            return Builder.SetAuthor(author).SetTitle("Notification").SetTime(DateTime.Now).SetText("New text").SetLevel(Category.INFO).SetColor(ConsoleColor.Yellow).Build();

        }
        public ConsoleNotification LogError(string author)
        {
            return Builder.SetAuthor(author).SetTitle("Notification").SetTime(DateTime.Now).SetText("New text").SetLevel(Category.ERROR).SetColor(ConsoleColor.Cyan).Build();

        }
        public ConsoleNotification LogAltr(string author)
        {
            return Builder.SetAuthor(author).SetTitle("Notification").SetTime(DateTime.Now).SetText("New text").SetLevel(Category.ALERT).SetColor(ConsoleColor.Green).Build();

        }
    }
}
