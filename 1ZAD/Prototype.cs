using System;
using System.Collections.Generic;
using System.Text;

namespace _1_ZAD
{
    interface Prototype
    {
        Prototype Clone();
    }
}
