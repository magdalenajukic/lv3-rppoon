using System;
using System.Collections.Generic;
using System.Text;

namespace _1_ZAD
{
    class Dataset: Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }

        public Prototype Clone()
        {
            Dataset deepCopy= (Dataset)this.MemberwiseClone();
            List <List<string>>dataCopy = new List<List<string>>();

            for(int i=0;i< data.Count; i++)
            {
                List<string> list = new List<string>();
                for(int j = 0; j < data[i].Count; j++)
                {
                    list.Add(data[i][j]);
                }
                dataCopy.Add(list);
            }
            deepCopy.data = dataCopy;
            return deepCopy;
        }

        public void ToString()
        {
            for (int i = 0; i < data.Count; i++)
            {
                for (int j = 0; j < data[i].Count; j++)
                {
                    Console.Write(data[i][j]);
                }
                Console.WriteLine(data);
               
            }

        }

    }
}