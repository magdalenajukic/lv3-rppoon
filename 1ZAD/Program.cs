using System;

namespace _1_ZAD
{
    class Program
    {
        static void Main(string[] args)
        {
            //C:\Users\Jukić\Desktop
            Dataset dataset = new Dataset(@"C:\Users\Jukić\Desktop\FILE.csv");
            Console.WriteLine("Original");
            dataset.ToString();
            Dataset dataset2 = (Dataset)dataset.Clone();
            Console.WriteLine("Copy");
            dataset2.ToString();
            //jednostavne tipove podatka nije potrebno duboko kopirati


        }
    }
}
