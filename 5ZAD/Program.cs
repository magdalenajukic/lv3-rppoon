using System;

namespace _5_ZAD
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetAuthor("New Author").SetText("New Text").SetColor(ConsoleColor.Red);
            NotificationManager menager = new NotificationManager();
            menager.Display(notificationBuilder.Build());

        }
    }
}