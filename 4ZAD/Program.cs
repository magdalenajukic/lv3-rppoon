using System;

namespace _4_ZAD
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification notification = new ConsoleNotification("Magdalena","Title","Text",DateTime.Now,Category.ERROR,ConsoleColor.Green);
            NotificationManager manager = new NotificationManager();
            manager.Display(notification);

        }
    }
}