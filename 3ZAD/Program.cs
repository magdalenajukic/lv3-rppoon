using System;

namespace _3_ZAD
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = Logger.GetInstance();
            logger.SetPath(@"C:\Users\Jukić\Desktop\log.txt");

            Logger logger2 = Logger.GetInstance();
            logger2.Log("New file!");
            Logger logger3 = Logger.GetInstance();
            logger3.Log("Is it new?");

        }
    }
}
