using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace _3_ZAD
{
    class Logger
    {
        //singlton je promjenj u kodu i tako se sejvo, trajno ga mijenjamo
        public static Logger instance;
        private string path;
        private Logger()
        {
            path = @"C:\Users\Jukić\Desktop\3zad.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void SetPath(string setpath)
        {
            path = setpath;
        }

        public void Log(string message)
        {
            StreamWriter file = new StreamWriter(path, true);
            file.Write(message);
            file.Close();
           

        }

    }
}
