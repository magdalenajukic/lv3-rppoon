using System;

namespace _7_ZAD
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification notification = new ConsoleNotification("Magdalena", "New Title", "New Text", DateTime.Now, Category.ERROR, ConsoleColor.Green);
            NotificationManager manager = new NotificationManager();
            ConsoleNotification clone = (ConsoleNotification)notification.Clone();
            manager.Display(notification);
            clone.SetColor(ConsoleColor.Yellow);
            manager.Display(clone);
            //nemamo šta duboko kopirat jer plitko kopiranje to napravi
            //nema potrebe duboko kopirat

        }
    }
}
