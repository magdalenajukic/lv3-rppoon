using System;
using System.Collections.Generic;
using System.Text;

namespace _7_ZAD
{
    public interface Prototype
    {
        Prototype Clone();
    }
}
