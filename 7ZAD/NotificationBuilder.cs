using System;
using System.Collections.Generic;
using System.Text;

namespace _7_ZAD
{
    class NotificationBuilder:IBuilder
    {
        string Author = " ";
        string Title = " ";
        string Text = " ";
        DateTime Time = DateTime.Now;
        Category Level = Category.ERROR;
        ConsoleColor Color = ConsoleColor.White;
        public ConsoleNotification Build()
        {
            return new ConsoleNotification(Author, Title, Text, Time, Level, Color);
        }

        public IBuilder SetAuthor(string author)
        {
            this.Author = author;
            return this;
        }

        public IBuilder SetColor(ConsoleColor color)
        {
            this.Color = color;
            return this;

        }

        public IBuilder SetLevel(Category level)
        {
            this.Level = level;
            return this;
        }

        public IBuilder SetText(string text)
        {
            this.Text = text;
            return this;
        }

        public IBuilder SetTime(DateTime time)
        {
            this.Time = time;
            return this;
        }

        public IBuilder SetTitle(string title)
        {
            this.Title = title;
            return this;
        }
    }
}

